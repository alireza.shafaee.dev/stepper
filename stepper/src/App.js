import './App.css';
import StepperContainer from './components/stepper/Stepper';
function App() {
  return (
    <div className="App">
      <StepperContainer/>
    </div>
  );
}

export default App;
