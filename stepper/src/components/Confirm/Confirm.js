import React from 'react'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import { ListItemText } from '@material-ui/core';
import {skillsChecks} from '../Forms/userInf/UserInf';
import { ACTIONS } from '../stepper/Stepper';
const Confirm=(props)=> {
     const skillArrays = skillsChecks.map(item=>item.value);
     console.log(skillArrays);
     const list = Object.keys(props.userData).map((item)=>{
       if(skillArrays.includes(item))
       {
         
         if(props.userData[item])
         {
          return <ListItem button>
            <ListItemText primary={item}/>
          </ListItem>
         }else{
           return null;
         }
       }else{
         return null;
       }
     });
     console.log(list);
    return ( 
    <Card style={{backgroundColor:' rgba(221, 221, 221, 0.301)' , width:500, margin:'auto', color:"#3F51B5"}}>
        <CardActionArea>
          <CardMedia
            title="Contemplative Reptile"
          />
          <div style={{textAlign:'center'}}>
            <img style={{maxHeight:300}} alt="b" src={props.imageUrl}/>
          </div>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
            {props.userData.userName}
            <br/>
            Email : {props.userData.email}
            <hr/>
            </Typography>
          <Typography style={{textAlign:'center'}} gutterBottom variant="h5" component="h2">
               {props.userData.job.label}
            <hr/>
            </Typography>
          <h2 style={{textAlign:"center"}}>your skills</h2>
          <List >
            {list}
         </List>
          </CardContent>
        </CardActionArea>
        <CardActions style={{backgroundColor:' rgba(221, 221, 221, 0.301)',textAlign:"center"}}>
        <Button
                    style={{width:'50%' ,marginRight:5,padding:"7px 1px" , backgroundColor:'white'}}
                    onClick={()=>{props.dispatch({type:ACTIONS.Prev})}}
                >
                    Back
                </Button>
          <Button size="big" color="primary" variant="contained"  style={{margin:'auto' ,width:"50%"}}>
            Confirm
          </Button>
        </CardActions>
      </Card>
    )
}

export default Confirm;
