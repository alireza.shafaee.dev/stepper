import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import React, {useReducer} from 'react';
import UserInf from '../Forms/userInf/UserInf';
import ImageUpload from '../Forms/ImageUpload/ImageUpload';
import Confirm from '../Confirm/Confirm';
export const ACTIONS={
    Next : 'next', 
    Prev : 'Previous',
    Reset : 'reset',
    SET_USER_DATA : "set user data",
    SET_IMAGE_URL : 'set image url'
}
const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    backButton: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    title: {
        flexGrow: 1,
      }
  }));
  
  function getSteps() {
    return ['User information', 'Personal information', 'Information confirm'];
  }
  
const reducer= (state , action)=>{
    switch(action.type)
    {
        case ACTIONS.Next:
            return {...state ,step:state.step+1};
        case ACTIONS.Prev:
            return {...state, step : state.step-1};
        case ACTIONS.Reset:
            return {...state , step : 0};
        case ACTIONS.SET_USER_DATA:
            return {...state ,userData : action.payload.data}
        case ACTIONS.SET_IMAGE_URL:
            return {...state , imageUrl : action.payload.url}
        default : 
            return state;
    }
} 
const StepperContainer=()=> {
    const classes = useStyles();
    const [state , dispatch] = useReducer(reducer , {step : 0  , userData:null, imageUrl : null});
    const steps = getSteps();
    if(state.userData)
    {
      console.log(state.userData , state.imageUrl);
    }
    const getStepContent = (stepIndex)=>{
        switch (stepIndex) {
          case 0:
            return <UserInf userInf={state.userData} dispatch={dispatch} step={state.step}/>;
          case 1:
            return <ImageUpload dispatch={dispatch} url={state.imageUrl}/>;
          case 2:
            return <Confirm imageUrl={state.imageUrl} dispatch={dispatch} userData={state.userData}/>;
          default:
            return 'Unknown stepIndex';
        }
      }
    return (
      <> 
        <AppBar position="fixed">
            <Toolbar>
            <Typography variant="h6" className={classes.title}>
                Stepper
            </Typography>
            </Toolbar>
        </AppBar>
        <div style={{marginBottom:60}}></div>
        <Stepper activeStep={state.step} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div style={{position:"relative" , width:"50%" , margin:"auto"}}>
          {state.step === steps.length ? (
            <div>
              <Typography className={classes.instructions}>All steps completed</Typography>
              <Button onClick={()=>dispatch({type:ACTIONS.Reset})}>Reset</Button>
            </div>
          ) : (
            <div>
              {getStepContent(state.step)}
            </div>
          )}
        </div>
      </>
    );
  }
export default StepperContainer;