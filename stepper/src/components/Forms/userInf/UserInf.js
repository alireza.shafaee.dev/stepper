import React from 'react';
import {Controller, useForm} from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import TextField from '@material-ui/core/TextField';
import * as yup from "yup";
import Select from 'react-select';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import {ACTIONS} from '../../stepper/Stepper';
import { FormControlLabel } from '@material-ui/core';
const options = [
    {value : 'font-end' , label : 'Front-end developer'},
    {value : 'Back-end' , label : 'Back-end developer'},
    {value : 'wordpress' , label : 'Wordpress developer'},
    {value : 'UX/UI' , label : 'UX/UI desinger'}
];
export const skillsChecks = [
    {value : 'Css' , label : 'Css'}, 
    {value : 'JS'  , label : 'Java script'},
    {value : 'PHP' , label : 'PHP'},
    {value : 'C++' , label : 'C++'},
    {value : 'Python' , label : 'Python'}
]
const schema = yup.object().shape({
    userName: yup.string().required(),
    email: yup.string().email().required(),
  });
const UserInf=(props)=>{
    const { handleSubmit,control} = useForm({
        resolver: yupResolver(schema)
    
});
    const onSubmit = (data) => 
    {
        console.log(data);
        props.dispatch({type:ACTIONS.Next});
        props.dispatch({type:ACTIONS.SET_USER_DATA,payload:{data : data}})
    }
    return (
    <div>
        <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
            <div>
                <InputLabel style={{marginBottom : 6}}>Select your job</InputLabel>
                <Controller
                    control={control}
                    name="job"
                    defaultValue={props.userInf ? props.userInf.job : options[0]}
                    render={({field})=>{
                        return <Select
                        {...field}
                        options={options}
                        theme={theme => ({
                            ...theme,
                            borderRadius: 0,
                            colors: {
                            ...theme.colors,
                            primary25: 'rgb(184, 215, 241)',
                            primary: '#3F51B5',
                            },
                        })}
                        />
                    }}
                />
            <br/>
            <InputLabel style={{marginBottom : 6}}>Select your skills</InputLabel>
            <div>
                <Controller
                    control={control}
                    name="skill"
                    render={({field})=>{
                        return skillsChecks.map((item , ind)=>{
                            return <FormControlLabel {...field} control={<Controller 
                                control={control}
                                name={item.value}
                                
                                defaultValue={props.userInf? props.userInf[item.value] : false}
                                render={({ field }) => {
                                    return <FormControlLabel
                                        control={
                                            <Checkbox
                                                name={item.value}
                                                color="primary"
                                                {...field}
                                            />
                                        }
                                    label={item.label}/>
                                }}
                            />} key={ind}/>
                        })
                        }}
/*return */       
                />  
            </div>
            {/**/}
                <br/>
                <Controller
                    control={control}
                    name="userName"
                    defaultValue={props.userInf? props.userInf.userName :""}
                    render={({ field:   { onChange , value }, fieldState: { error } })=>{
                        return ( <TextField 
                            label="username" 
                            style={{width:"100%"}}
                            value={value}
                            onChange={onChange}
                            error={error}
                            helperText={error ? error.message: null}/>)
                    }}
                />
            </div>
            <br/>
            <div>
                <Controller
                    control={control}
                    name="email"
                    defaultValue={props.userInf? props.userInf.email :""}
                    render={({ field: { onChange, value }, fieldState: { error } })=>{
                        return ( <TextField 
                            id="standard-required" 
                            label="email" 
                            value={value}
                            style={{width:"100%"}}
                            onChange={onChange}
                            error={error}
                            helperText={error ? error.message: null}/>)
                    }}
                />
            </div>
            <br/>
            <div>
                <Button 
                variant="contained" 
                color="primary"
                type="submit">
                  {props.step ===  2 ? 'Finish' : 'Next'}
                </Button>
              </div>
        </form> 
    </div>
    )
}

export default UserInf;
