import React from 'react';
import {ACTIONS} from '../../stepper/Stepper';
import Container from '@material-ui/core/Container';
import InputLabel from '@material-ui/core/InputLabel';
import './ImageUpload.css'
import { Button } from '@material-ui/core';
const ImageUpload =(props)=> {
    return (
        <div>
            <form>
                <InputLabel>    
                    Upload your Image : 
                </InputLabel>
                <br/>
                <label className={"custom-file-upload"}>
                    <input alt="hello" type="file" onChange={(event)=>{
                            props.dispatch({type:ACTIONS.SET_IMAGE_URL , payload : {url :URL.createObjectURL(event.target.files[0])}});
                        }} accept="image/*"/>
                        Image
                </label>
                <br/>
            </form> 
                {props.url ? 
                <Container style={{position:"relative"}}>
                    <img style={{maxHeight:200,maxWidth:700,margin:"auto",display:"block" }}  src={props.url}   alt="s"/>
                </Container> : 
                    <div style={{backgroundColor : "rgb(190, 190, 190)" 
                                , color:'white' , 
                                borderRadius:"2px",
                                width:300,
                                margin:'auto',
                                textAlign:'center',
                                padding:"100px 30px"}}>
                        Image Preview
                    </div>
                }
                <br/>
            <div>
                <Button
                    style={{marginRight:5 , marginLeft:4, padding:"7px 1px"}}
                    onClick={()=>{props.dispatch({type:ACTIONS.Prev})}}
                >
                    Back
                </Button>
            <Button 
                variant="contained" 
                color="primary"
                type="submit"
                disabled={!props.url}
                onClick={()=>{props.dispatch({type:ACTIONS.Next})}}>
                    Next
                </Button>
              </div>                
        </div>
    )
}

export default ImageUpload;
